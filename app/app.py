import traceback
from static import leercsv
from flask import jsonify
from flask import Flask

app = Flask(__name__)

@app.route('/estudiantes')
def estructuraJsonEstudiantes():
    try:
        cargaCsv = leercsv.cargaCSV("ESTUDIANTES")
        if cargaCsv['estado'] == "OK":
            datos = {
                "codigo": 200,
                "data": cargaCsv["data"]
            }
        else:
            datos = {
                "codigo": 400,
                "data": {
                    "error": "Ha ocurrido una excepcion"
                }
            }
    except Exception as e:
        traceback.print_exc()
        datos = {
            "codigo": 400,
            "data": {
                "error": "Ha ocurrido una excepcion",
                "codigo-error": e
            }
        }
    return jsonify(datos)

@app.route('/asignaturas')
def estructuraJsonAsignaturas():
    try:
        cargaCsv = leercsv.cargaCSV("ASIGNATURAS")
        if cargaCsv['estado'] == "OK":
            datos = {
                "codigo": 200,
                "data": cargaCsv["data"]
            }
        else:
            datos = {
                "codigo": 400,
                "data": {
                    "error": "Ha ocurrido una excepcion"
                }
            }
    except Exception as e:
        traceback.print_exc()
        datos = {
            "codigo": 400,
            "data": {
                "error": "Ha ocurrido una excepcion",
                "codigo-error": e
            }
        }
    return jsonify(datos)


if __name__ == '__main__':
    app.run()
