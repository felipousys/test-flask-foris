# -*- coding: utf-8 -*-
import csv
import traceback


def cargaCSV(tipo):
    try:
        listaEstudiante = {}
        listaalumnos = []
        listadoAsignaturas = []
        listaAsignatura = {}
        nombreArchivo = "foris.csv"
        with open(nombreArchivo, 'r') as file:
            reader = csv.DictReader(file)
            for filas in reader:
                if (tipo == "ESTUDIANTES"):
                    llenar = llenarCargaAcademica(filas)
                    if (filas['ESTUDIANTE'] not in listaalumnos):
                        listaalumnos.append(filas['ESTUDIANTE'])
                        estudiantes = {
                                "datos-estudiante": {
                                    "id-estudiante": filas['ESTUDIANTE'],
                                    "min-creo-co": filas['MIN_CRE_CO'],
                                    "estado": filas['ESTADO']
                                },
                                "asignaturas": {}
                            }
                        listaEstudiante.update({filas['ESTUDIANTE']: estudiantes})
                    buscaryAgregar(llenar, listaEstudiante[filas['ESTUDIANTE']], filas['COD_ASIGNA'])
                elif (tipo == "ASIGNATURAS"):
                    listadoAsignaturas.append(filas['COD_ASIGNA'])
                    llenar = llenarCargaEstudiantes(filas)
                    if (filas['COD_ASIGNA'] not in listaAsignatura):
                        asignatura = {
                            "datos-asignatura": {
                                    "anio": filas['CUR_ACADEM'],
                                    "codigo-asignatura": filas['COD_ASIGNA'],
                                    "nombre-asignatura": filas['NOMBRE'],
                                    "numero-grupo": filas['NUM_GRUPO'],
                                    "turno": filas['TURNO'],
                                    "fecha-matricula": filas['FEC_MAT'],
                                    "codigo-titulo": filas['COD_TITULO'],
                                    "codigo-plan": filas['COD_PLAN'],
                                    "cod-edicio": filas['COD_EDICIO']
                            },
                            "alumnos-registrados": {}
                        }
                        listaAsignatura.update({filas['COD_ASIGNA']: asignatura})
                    buscaryAgregarAlumnos(llenar, listaAsignatura[filas['COD_ASIGNA']], filas['ESTUDIANTE'])

        if (tipo == "ASIGNATURAS"):
            datos = listaAsignatura
        else:
            datos = listaEstudiante

        infoCSV = {
            "estado": "OK",
            "data": datos
        }
    except Exception as e:
        traceback.print_exc()
        infoCSV = {
            "estado": "NO-OK",
            "data": None
        }
    return (infoCSV)


def llenarCargaAcademica(filas):
    try:
        cargaPorAlumno = {}
        cargaAcademica = {
                    "anio": filas['CUR_ACADEM'],
                    "codigo-asignatura": filas['COD_ASIGNA'],
                    "nombre-asignatura": filas['NOMBRE'],
                    "numero-grupo": filas['NUM_GRUPO'],
                    "turno": filas['TURNO'],
                    "fecha-matricula": filas['FEC_MAT'],
                    "codigo-titulo": filas['COD_TITULO'],
                    "codigo-plan": filas['COD_PLAN'],
                    "cod-edicio": filas['COD_EDICIO'],
        }
        cargaPorAlumno = cargaAcademica
    except Exception as e:
        traceback.print_exc()
        cargaPorAlumno = None
    return (cargaPorAlumno)

def llenarCargaEstudiantes(filas):
    try:
        cargaPorAlumno = {}
        detalleAlumno = {
            "id-estudiante": filas['ESTUDIANTE'],
            "min-creo-co": filas['MIN_CRE_CO'],
            "estado": filas['ESTADO']
        }
        cargaPorAlumno = detalleAlumno
    except Exception as e:
        traceback.print_exc()
        cargaPorAlumno = None
    return (cargaPorAlumno)


def buscaryAgregar(llenar, listaEstudiante, idAsignatura):
    try:
        listaEstudiante['asignaturas'].update({idAsignatura: llenar})
        salida = True
    except Exception as e:
        traceback.print_exc()
        salida = None
    return (salida)


def buscaryAgregarAlumnos(llenar, listaAsignatura, idAlumno):
    try:
        listaAsignatura['alumnos-registrados'].update({idAlumno: llenar})
        salida = True
    except Exception as e:
        traceback.print_exc()
        salida = None
    return (salida)


def generarAlumnos(filas, listaEstudiantes):
    try:
        codigoAlumno = dict()
        if len(listaEstudiantes) == 0:
            codigoAlumno[filas['ESTUDIANTE']] = {
                "codigo-estudiante": filas['ESTUDIANTE'],
                "datos-estudiante": {
                    "min-creo-co": filas['MIN_CRE_CO'],
                    "estado": filas['ESTADO']
                },
                "asignaturas": []
            }
        else:
            for codigoEstudiante in listaEstudiantes:
                if codigoEstudiante['codigo-estudiante'] not in filas['ESTUDIANTE']:
                    codigoAlumno[filas['ESTUDIANTE']] = {
                        "codigo-estudiante": filas['ESTUDIANTE'],
                        "datos-estudiante": {
                            "min-creo-co": filas['MIN_CRE_CO'],
                            "estado": filas['ESTADO']
                        },
                        "asignaturas": []
                    }
    except Exception as e:
        traceback.print_exc()
        codigoAlumno = None
    return (codigoAlumno)
